const express = require('express');
const { Server } = require('ws');


const PORT = 8099;
const HOME = '/index.html';

const server = express()
  .use((req, res) => res.sendFile(HOME, { root: __dirname }))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

const ws = new Server({ server }); // creer le serveur websocket

// traitement à la connection
ws.on('connection', (socket) => {

  socket.on('close', () => console.log('Client disconnected'));
});

// envoie l'heure aux clients toute les secondes
setInterval(() => {
  ws.clients.forEach((client) => {
    client.send(new Date().toTimeString());
  });
}, 1000);
